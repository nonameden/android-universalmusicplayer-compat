package com.example.android.uamp.utils;

import android.app.Activity;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.widget.ImageView;

/**
 * Created by Denis Feoktistov on 09 Apr 2015.
 */
public class DrawableHelper {

    public static Drawable getDrawable(Activity activity, int resId) {
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            return activity.getResources().getDrawable(resId);
        } else {
            return activity.getDrawable(resId);
        }
    }

    public static void setImageTintList(ImageView imageView, ColorStateList colorList) {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            imageView.setImageTintList(colorList);
        }
    }
}
