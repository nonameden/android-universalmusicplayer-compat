package com.example.android.uamp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.view.KeyEvent;

import com.example.android.uamp.shared.Constants;

/**
 * Created by Denis Feoktistov on 09 Apr 2015.
 */
public class RemoteControlClientButtonReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Constants.ACTION_MEDIA_BUTTONS)) {
            final KeyEvent event = intent.getParcelableExtra(Intent.EXTRA_KEY_EVENT);

            if (event != null && event.getAction() == KeyEvent.ACTION_UP) {
                switch (event.getKeyCode()) {
                    case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE:
                        onServiceAction(context, MediaNotificationManager.ACTION_PLAY_PAUSE);
                        break;
                    case KeyEvent.KEYCODE_MEDIA_PAUSE:
                        onServiceAction(context, MediaNotificationManager.ACTION_PAUSE);
                        break;
                    case KeyEvent.KEYCODE_MEDIA_PLAY:
                        onServiceAction(context, MediaNotificationManager.ACTION_PLAY);
                        break;
                    case KeyEvent.KEYCODE_MEDIA_PREVIOUS:
                        onServiceAction(context, MediaNotificationManager.ACTION_PREV);
                        break;
                    case KeyEvent.KEYCODE_MEDIA_NEXT:
                        onServiceAction(context, MediaNotificationManager.ACTION_NEXT);
                        break;
                    case KeyEvent.KEYCODE_MEDIA_STOP:
                        onServiceAction(context, MediaNotificationManager.ACTION_STOP);
                        break;
                }
            }
        }
    }

    private void onServiceAction(Context context, String action) {
        context.sendBroadcast(new Intent(action));
    }
}